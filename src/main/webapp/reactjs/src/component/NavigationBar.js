import React from 'react';
import {Navbar, Nav} from 'react-bootstrap';
import {Link} from 'react-router-dom';
class NavigationBar extends React.Component{


     render(){
        return(
        <div>
            <Navbar bg="dark" variant="dark">
            <Link to={"home"} className="navbar-brand">BookShop</Link>

            <Nav className="mr-auto">
                  <Link to={"home"} className="nav-link">Home</Link>
                  <Link to={"add"} className="nav-link">AddEmployee</Link>
                  <Link to={"list"}className="nav-link">Employee List</Link>
                  <Link to={"users"}className="nav-link">Users</Link>
                 
                </Nav>
                </Navbar>
        </div>
        )
     }

}  


export default NavigationBar;