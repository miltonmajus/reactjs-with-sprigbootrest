import React from 'react';
import './App.css';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import {Container, Row, Col} from 'react-bootstrap';
import NavigationBar from './component/NavigationBar';
import Welcome from './component/Welcome';
import BookList from './component/BookList';
import Book from './component/Book';
import UserList from './component/UserList';
import Footer from './component/Footer';

function App() {

 const marginTop = {
  marginTop:"20px"
 };
  return (
    <Router>
     <NavigationBar/>
        <Container>
        <Row>
        <Col lg={12} style={marginTop}>
        <Switch>
         <Route path="/home" exact component={Welcome}/>
         <Route path="/list" exact component={BookList}/>
         <Route path="/edit/:id" exact component={Book}/>
         <Route path="/add" exact component={Book}/>
         <Route path="/users" exact component={UserList}/>
         
         
            </Switch>

        </Col>
        </Row>
        </Container>

        <Footer/>
    </Router>
  );
}

export default App;
